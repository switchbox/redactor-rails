# Redactor Rails #

This is the Redactor gem for Switchbox Inc.


### Installation ###

**In your Gemfile**

```
#!ruby


gem 'redactor_rails', git: 'git@bitbucket.org:switchbox/redactor-rails.git', :tag => 'tag_number'
```


**applicaton.js**
```
#!javascript

//= require redactor_rails
```


**application.scss**
```
#!scss

@import "redactor_rails";
```

### Usage ###

After installation, simply create a textarea with the class "redactor".

```
#!HTML

<textarea cols="80" class="redactor" id="" name="redactor_text" rows="20"></textarea>
```
