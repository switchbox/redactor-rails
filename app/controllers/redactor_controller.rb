class RedactorController < ApplicationController

  skip_before_action :verify_authenticity_token

  def images
    images = []
    types = ['png', 'jpg', 'jpeg', 'tiff', 'gif', 'bmp']
    Dir.foreach('public/uploads/images') do |file|
      if file.split('.').last.in?(types) && file.split('_').first != 'thumb'
        img = ImageUploader.new
        img.retrieve_from_store!(file)
        images << {
          thumb: img.thumb.url,
          url: img.url,
          title: file,
          id: SecureRandom.uuid
        }
      end
    end
    render json: images
  end

  def image_upload
    @file = ImageUploader.new
    @file.store!(params[:file])
    render json: [{ success: true, url: @file.url }]
  end

  def files
    files = []
    types = ['doc', 'docx', 'pdf', 'pages']
    Dir.foreach('public/uploads/files') do |file|
      if file.split('.').last.in?(types)
        f = FileUploader.new
        f.retrieve_from_store!(file)
        files << {
          url: f.url,
          title: file,
          name: file,
          size: f.size,
          id: SecureRandom.uuid
        }
      end
    end
    render json: files
  end

  def file_upload
    @file = FileUploader.new
    @file.store!(params[:file])
    render json: [{ success: true, url: @file.url, name: @file.filename }]
  end

  private

end
