$(document).ready(function() {
    $(".redactor").redactor({
        imageUpload: '/redactor/image_upload',
        imageManagerJson: '/redactor/images',
        fileUpload: '/redactor/file_upload',
        fileManagerJson: '/redactor/files',
        plugins: [
            'imagemanager', 'filemanager', 'source',
            'alignment', 'table', 'video'
        ]
    });
});
