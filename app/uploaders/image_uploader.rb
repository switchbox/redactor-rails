# encoding: utf-8

class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  version :thumb do
    process resize_to_fill: [100, 100]
  end

  storage :file

  def store_dir
    "uploads/images"
  end

end
