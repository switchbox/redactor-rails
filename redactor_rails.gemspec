$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "redactor_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "redactor_rails"
  s.version     = RedactorRails::VERSION
  s.authors     = ["Tyler Steiman"]
  s.email       = ["tsteiman@switchboxinc.com"]
  s.homepage    = "http://www.switchboxinc.com"
  s.summary     = "The Switchbox Redactor Gem for Rails"
  s.description = "This is the main project from the redactor rails gem for Switchbox Inc."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.2.11.3"
  s.add_dependency 'jquery-rails', '~> 4.2', '>= 4.2.1'
  s.add_dependency 'carrierwave', '~> 0.11.2'
  s.add_dependency 'mini_magick', '~> 4.5', '>= 4.5.1'

  s.add_development_dependency "sqlite3"
end
