Rails.application.routes.draw do
  get 'redactor/images' => 'redactor#images'
  post 'redactor/image_upload' => 'redactor#image_upload'

  get 'redactor/files' => 'redactor#files'
  post 'redactor/file_upload' => 'redactor#file_upload'
end
