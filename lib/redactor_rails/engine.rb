require 'jquery-rails'
require 'carrierwave'
require 'mini_magick'

module RedactorRails
  class Engine < ::Rails::Engine
    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end

    initializer :assets do |config|
      Rails.application.config.assets.precompile += %w{ redactor_rails.js }
      Rails.application.config.assets.precompile += %w{ redactor_rails.scss }
    end
  end
end
